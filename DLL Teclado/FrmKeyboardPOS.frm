VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmKeyboardPOS 
   Appearance      =   0  'Flat
   BackColor       =   &H00404040&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5130
   ClientLeft      =   315
   ClientTop       =   615
   ClientWidth     =   15225
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   15225
   Begin VB.Frame NumPad 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4395
      Left            =   11760
      TabIndex        =   66
      Top             =   600
      Width           =   3365
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "0"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   309
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   77
         Tag             =   "0"
         Top             =   3396
         Width           =   2204
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "7"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   300
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   76
         Tag             =   "7"
         Top             =   2264
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "4"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   303
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   75
         Tag             =   "4"
         Top             =   1132
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "1"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   306
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   74
         Tag             =   "1"
         Top             =   0
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "8"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   301
         Left            =   1132
         Style           =   1  'Graphical
         TabIndex        =   73
         Tag             =   "8"
         Top             =   2264
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "5"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   304
         Left            =   1132
         Style           =   1  'Graphical
         TabIndex        =   72
         Tag             =   "5"
         Top             =   1132
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "2"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   307
         Left            =   1132
         Style           =   1  'Graphical
         TabIndex        =   71
         Tag             =   "2"
         Top             =   0
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "9"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   302
         Left            =   2264
         Style           =   1  'Graphical
         TabIndex        =   70
         Tag             =   "9"
         Top             =   2264
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "6"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   305
         Left            =   2264
         Style           =   1  'Graphical
         TabIndex        =   69
         Tag             =   "6"
         Top             =   1132
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00E0E0E0&
         Caption         =   "3"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1072
         Index           =   308
         Left            =   2264
         Style           =   1  'Graphical
         TabIndex        =   68
         Tag             =   "3"
         Top             =   0
         Width           =   1072
      End
      Begin VB.CommandButton CmdKey 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Caption         =   "."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   405
         Left            =   2264
         MaskColor       =   &H00E0E0E0&
         Style           =   1  'Graphical
         TabIndex        =   67
         Tag             =   "."
         Top             =   3396
         Width           =   1072
      End
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "/"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   28
      Left            =   14040
      TabIndex        =   65
      Top             =   5400
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   259
      Left            =   10515
      Style           =   1  'Graphical
      TabIndex        =   64
      Tag             =   "#"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   258
      Left            =   9585
      Style           =   1  'Graphical
      TabIndex        =   63
      Tag             =   ":"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "\"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   257
      Left            =   8655
      Style           =   1  'Graphical
      TabIndex        =   62
      Tag             =   "\"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "&&"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   256
      Left            =   7725
      Style           =   1  'Graphical
      TabIndex        =   61
      Tag             =   "&"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   ";"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   255
      Left            =   6795
      Style           =   1  'Graphical
      TabIndex        =   60
      Tag             =   ";"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "$"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   254
      Left            =   5865
      Style           =   1  'Graphical
      TabIndex        =   59
      Tag             =   "$"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "@"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   253
      Left            =   4935
      Style           =   1  'Graphical
      TabIndex        =   58
      Tag             =   "@"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "_"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   252
      Left            =   4005
      Style           =   1  'Graphical
      TabIndex        =   57
      Tag             =   "_"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   251
      Left            =   3075
      Style           =   1  'Graphical
      TabIndex        =   56
      Tag             =   "*"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   250
      Left            =   2145
      Style           =   1  'Graphical
      TabIndex        =   55
      Tag             =   "-"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "+"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   249
      Left            =   1215
      Style           =   1  'Graphical
      TabIndex        =   54
      Tag             =   "+"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton ActivarNum 
      Caption         =   "Symbols"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10110
      TabIndex        =   53
      Top             =   4245
      Width           =   1485
   End
   Begin VB.CommandButton CmdKey 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   ","
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   406
      Left            =   9150
      MaskColor       =   &H00E0E0E0&
      Style           =   1  'Graphical
      TabIndex        =   52
      Tag             =   ","
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00E0E0E0&
      Caption         =   "/"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Index           =   248
      Left            =   285
      Style           =   1  'Graphical
      TabIndex        =   51
      Tag             =   "/"
      Top             =   580
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   247
      Left            =   8190
      TabIndex        =   50
      Tag             =   "%"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   17
      Left            =   30
      Picture         =   "FrmKeyboardPOS.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   49
      Top             =   5820
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Shift"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Index           =   16
      Left            =   45
      TabIndex        =   48
      Tag             =   "Shift"
      Top             =   5340
      Width           =   1215
   End
   Begin VB.CommandButton CmdKey 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   "Esc"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   15
      Left            =   1485
      Style           =   1  'Graphical
      TabIndex        =   47
      Tag             =   "Esc"
      Top             =   5655
      Width           =   840
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Num"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   14
      Left            =   11535
      TabIndex        =   46
      Tag             =   "Num"
      Top             =   5295
      Width           =   840
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H0071CC2E&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   13
      Left            =   165
      Picture         =   "FrmKeyboardPOS.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   45
      Tag             =   "Caps"
      Top             =   3300
      Width           =   1270
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Supr"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   12
      Left            =   14190
      TabIndex        =   44
      Tag             =   "Supr"
      Top             =   6015
      Width           =   840
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Insert"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Index           =   11
      Left            =   8085
      TabIndex        =   43
      Top             =   6120
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Fin"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Index           =   10
      Left            =   7110
      TabIndex        =   42
      Top             =   6120
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Inicio"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Index           =   9
      Left            =   6150
      TabIndex        =   41
      Top             =   6120
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.CommandButton CmdKey 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Wingdings 3"
         Size            =   20.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   8
      Left            =   9150
      Picture         =   "FrmKeyboardPOS.frx":2CC4
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   4245
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Wingdings 3"
         Size            =   20.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   7
      Left            =   8190
      Picture         =   "FrmKeyboardPOS.frx":302A
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   4245
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Wingdings 3"
         Size            =   20.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Index           =   6
      Left            =   13125
      Picture         =   "FrmKeyboardPOS.frx":3372
      Style           =   1  'Graphical
      TabIndex        =   38
      Top             =   6165
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Alt"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   4
      Left            =   4230
      TabIndex        =   36
      Tag             =   "Alt"
      Top             =   5925
      Width           =   1295
   End
   Begin VB.CommandButton CmdKey 
      Caption         =   "Ctrl"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   3
      Left            =   165
      TabIndex        =   35
      Tag             =   "Ctrl"
      Top             =   4245
      Width           =   1275
   End
   Begin VB.Timer ActLeds 
      Interval        =   200
      Left            =   7275
      Top             =   75
   End
   Begin VB.TextBox TxtBuffer 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   6375
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   5400
      Width           =   3255
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   246
      Left            =   7230
      Picture         =   "FrmKeyboardPOS.frx":3703
      Style           =   1  'Graphical
      TabIndex        =   29
      Tag             =   "M"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Wingdings 3"
         Size            =   27.75
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1840
      Index           =   1
      Left            =   10110
      Picture         =   "FrmKeyboardPOS.frx":4C89
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   2370
      Width           =   1485
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   2
      Left            =   1470
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   4245
      Width           =   6675
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Wingdings 3"
         Size            =   26.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   0
      Left            =   9750
      Picture         =   "FrmKeyboardPOS.frx":50B2
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1440
      Width           =   1860
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   232
      Left            =   2430
      Picture         =   "FrmKeyboardPOS.frx":5482
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "D"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   239
      Left            =   9150
      Picture         =   "FrmKeyboardPOS.frx":6A08
      Style           =   1  'Graphical
      TabIndex        =   22
      Tag             =   "�"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   245
      Left            =   6270
      Picture         =   "FrmKeyboardPOS.frx":7F8E
      Style           =   1  'Graphical
      TabIndex        =   28
      Tag             =   "N"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   244
      Left            =   5310
      Picture         =   "FrmKeyboardPOS.frx":9514
      Style           =   1  'Graphical
      TabIndex        =   27
      Tag             =   "B"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   225
      Left            =   4950
      Picture         =   "FrmKeyboardPOS.frx":AA9A
      Style           =   1  'Graphical
      TabIndex        =   7
      Tag             =   "y"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   223
      Left            =   3030
      Picture         =   "FrmKeyboardPOS.frx":C020
      Style           =   1  'Graphical
      TabIndex        =   5
      Tag             =   "r"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   229
      Left            =   8790
      Picture         =   "FrmKeyboardPOS.frx":D5A6
      Style           =   1  'Graphical
      TabIndex        =   11
      Tag             =   "p"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   236
      Left            =   6270
      Picture         =   "FrmKeyboardPOS.frx":EB2C
      Style           =   1  'Graphical
      TabIndex        =   19
      Tag             =   "J"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   243
      Left            =   4350
      Picture         =   "FrmKeyboardPOS.frx":100B2
      Style           =   1  'Graphical
      TabIndex        =   26
      Tag             =   "V"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   224
      Left            =   3990
      Picture         =   "FrmKeyboardPOS.frx":11638
      Style           =   1  'Graphical
      TabIndex        =   6
      Tag             =   "t"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   230
      Left            =   510
      Picture         =   "FrmKeyboardPOS.frx":12BBE
      Style           =   1  'Graphical
      TabIndex        =   13
      Tag             =   "A"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   237
      Left            =   7230
      Picture         =   "FrmKeyboardPOS.frx":14144
      Style           =   1  'Graphical
      TabIndex        =   20
      Tag             =   "K"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   231
      Left            =   1470
      Picture         =   "FrmKeyboardPOS.frx":156CA
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "S"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   238
      Left            =   8190
      Picture         =   "FrmKeyboardPOS.frx":16C50
      Style           =   1  'Graphical
      TabIndex        =   21
      Tag             =   "L"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   222
      Left            =   2070
      Picture         =   "FrmKeyboardPOS.frx":181D6
      Style           =   1  'Graphical
      TabIndex        =   4
      Tag             =   "e"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BackColor       =   &H00808080&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   220
      Left            =   165
      Picture         =   "FrmKeyboardPOS.frx":1975C
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "q"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   226
      Left            =   5910
      Picture         =   "FrmKeyboardPOS.frx":1ACE2
      Style           =   1  'Graphical
      TabIndex        =   8
      Tag             =   "u"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   233
      Left            =   3390
      Picture         =   "FrmKeyboardPOS.frx":1C268
      Style           =   1  'Graphical
      TabIndex        =   16
      Tag             =   "F"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   240
      Left            =   1470
      Picture         =   "FrmKeyboardPOS.frx":1D7EE
      Style           =   1  'Graphical
      TabIndex        =   23
      Tag             =   "Z"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   221
      Left            =   1110
      Picture         =   "FrmKeyboardPOS.frx":1ED74
      Style           =   1  'Graphical
      TabIndex        =   3
      Tag             =   "w"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   227
      Left            =   6870
      Picture         =   "FrmKeyboardPOS.frx":202FA
      Style           =   1  'Graphical
      TabIndex        =   9
      Tag             =   "i"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   234
      Left            =   4350
      Picture         =   "FrmKeyboardPOS.frx":21880
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "G"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   241
      Left            =   2430
      Picture         =   "FrmKeyboardPOS.frx":22E06
      Style           =   1  'Graphical
      TabIndex        =   24
      Tag             =   "X"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   228
      Left            =   7830
      Picture         =   "FrmKeyboardPOS.frx":2438C
      Style           =   1  'Graphical
      TabIndex        =   10
      Tag             =   "o"
      Top             =   1440
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   235
      Left            =   5310
      Picture         =   "FrmKeyboardPOS.frx":25912
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "H"
      Top             =   2370
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   242
      Left            =   3390
      Picture         =   "FrmKeyboardPOS.frx":26E98
      Style           =   1  'Graphical
      TabIndex        =   25
      Tag             =   "C"
      Top             =   3300
      Width           =   900
   End
   Begin VB.CommandButton CmdKey 
      BeginProperty Font 
         Name            =   "Wingdings 3"
         Size            =   20.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Index           =   5
      Left            =   13125
      Picture         =   "FrmKeyboardPOS.frx":2841E
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   5280
      Visible         =   0   'False
      Width           =   825
   End
   Begin MSComctlLib.ImageList Teclas 
      Left            =   6600
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   65
      ImageHeight     =   65
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   58
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":287E4
            Key             =   "Q"
            Object.Tag             =   "Q"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":29D7A
            Key             =   "QMin"
            Object.Tag             =   "QMin"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":2B310
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":2C8A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":2DE3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":2F3D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":30968
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":31EFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":33494
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":34A2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":35FC0
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":37556
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":38AEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":3A082
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":3B618
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":3CBAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":3E144
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":3F6DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":40C70
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":42206
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4379C
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":44D32
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":462C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4785E
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":48DF4
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4A38A
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4B920
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4CEB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4E44C
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":4F9E2
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":50F78
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5250E
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":53AA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5503A
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":565D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":57B66
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":590FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5A692
            Key             =   ""
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5BC28
            Key             =   ""
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5D1BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5E754
            Key             =   ""
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":5FCEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":61280
            Key             =   ""
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":62816
            Key             =   ""
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":63DAC
            Key             =   ""
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":65342
            Key             =   ""
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":668D8
            Key             =   ""
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":67E6E
            Key             =   ""
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":69404
            Key             =   ""
         EndProperty
         BeginProperty ListImage50 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":6A99A
            Key             =   ""
         EndProperty
         BeginProperty ListImage51 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":6BF30
            Key             =   ""
         EndProperty
         BeginProperty ListImage52 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":6D4C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage53 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":6EA5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage54 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":6FFF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage55 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":71588
            Key             =   ""
         EndProperty
         BeginProperty ListImage56 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":724DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage57 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":7342C
            Key             =   ""
         EndProperty
         BeginProperty ListImage58 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKeyboardPOS.frx":73C7E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblFin 
      Alignment       =   2  'Center
      BackColor       =   &H00404040&
      Caption         =   "FIN"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000B&
      Height          =   375
      Left            =   9000
      TabIndex        =   81
      Tag             =   "Esc"
      Top             =   60
      Width           =   900
   End
   Begin VB.Image KeyboardSettings 
      Height          =   720
      Left            =   120
      Picture         =   "FrmKeyboardPOS.frx":744D0
      Top             =   -60
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label lblEscape 
      Alignment       =   2  'Center
      BackColor       =   &H00404040&
      Caption         =   "ESC"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000B&
      Height          =   375
      Left            =   9840
      TabIndex        =   79
      Tag             =   "Esc"
      Top             =   60
      Width           =   1275
   End
   Begin VB.Image AddNewLine 
      Height          =   480
      Left            =   11280
      Picture         =   "FrmKeyboardPOS.frx":7A4C2
      Top             =   45
      Width           =   480
   End
   Begin VB.Image Clear 
      Height          =   480
      Left            =   12240
      Picture         =   "FrmKeyboardPOS.frx":7A935
      Top             =   45
      Width           =   480
   End
   Begin VB.Label lblTitulo 
      BackStyle       =   0  'Transparent
      Caption         =   "Stellar Keyboard"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   960
      TabIndex        =   78
      Top             =   175
      Width           =   2655
   End
   Begin VB.Image CambiarPosicion 
      Height          =   480
      Left            =   13320
      Picture         =   "FrmKeyboardPOS.frx":7B177
      Top             =   60
      Width           =   480
   End
   Begin VB.Image Salir 
      Height          =   480
      Left            =   14400
      Picture         =   "FrmKeyboardPOS.frx":7B9B9
      Top             =   75
      Width           =   480
   End
   Begin VB.Label LblInsert 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Insert"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   195
      Left            =   10875
      TabIndex        =   34
      Top             =   660
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Label LblCapsLock 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000D&
      Caption         =   " "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   840
      Left            =   2460
      TabIndex        =   33
      Tag             =   "CapsLock"
      Top             =   5730
      Width           =   240
   End
   Begin VB.Label LblNumLock 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000D&
      Caption         =   "NumLock"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   12045
      TabIndex        =   32
      Top             =   5940
      Visible         =   0   'False
      Width           =   900
   End
   Begin VB.Label LblScrollLock 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ScrollLock"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   195
      Left            =   11355
      TabIndex        =   31
      Top             =   660
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Label lblIni 
      Alignment       =   2  'Center
      BackColor       =   &H00404040&
      Caption         =   "INI"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000B&
      Height          =   375
      Left            =   7920
      TabIndex        =   80
      Tag             =   "Esc"
      Top             =   60
      Width           =   900
   End
End
Attribute VB_Name = "FrmKeyboardPOS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public MskAlt As Boolean, MskCtrl As Boolean, MskShift As Boolean
Public PosActText As Integer
Public OriginalText As String
Public KeyboardSettingsObject

Private OriginalNumLockState As Integer
Private OriginalBloqMayusState As Integer
Private OriginalInsertState As Integer
Private OriginalHideSel As Boolean

Private Const ABM_GETTASKBARPOS = &H5

Dim ClaseRutinas As New cls_Rutinas

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hWnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" (ByVal dwMessage As Long, pData As APPBARDATA) As Long

Private Declare Function GetDeviceCaps Lib "gdi32" _
        (ByVal hdc As Long, ByVal nIndex As Long) As Long

Const HorizontalResolution = 8
Const VerticalResolution = 10

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub lblFin_Click()
    CmdKey_Click 10
End Sub

Private Sub lblIni_Click()
    CmdKey_Click 9
End Sub

Private Sub LblTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Form_MouseMove Button, Shift, X, Y
End Sub

Private Sub ActivarNum_Click()
    For I = 248 To 259
        CmdKey(I).Visible = Not CmdKey(I).Visible
    Next
End Sub

Private Sub AddNewLine_Click()
    If (UCase(TypeName(ObjText)) = UCase("TextBox")) Then
        Call InsertarEnPosicion(TxtBuffer, PosActText, vbNewLine)
        Call EstablecerMsk
        ObjText = TxtBuffer
        Call Me.MoverAPosicion(TxtBuffer, Len(ObjText.Text))
    End If
End Sub

Private Sub CambiarPosicion_Click()

    If Me.Top = 0 Then
        'Me.Top = Screen.Height - FrmKeyboardPOS.Height
        Dim TskH
        TskH = (GetTaskBarHeight * Screen.TwipsPerPixelY)
        If TskH = Screen.Height Then TskH = 0
        Me.Top = Screen.Height - FrmKeyboardPOS.Height - TskH
    Else
        Me.Top = 0
    End If
    
    If FrmKeyboardPOS.Top > 0 Then
        CambiarPosicion.Picture = Teclas.ListImages(58).Picture
    Else
        CambiarPosicion.Picture = Teclas.ListImages(57).Picture
    End If
    
End Sub

Private Sub ActLeds_Timer()
    
    LblCapsLock.Visible = EventoTeclado.GetKeyState(vbKeyCapital) = 1
    'LblScrollLock.Visible = EventoTeclado.GetKeyState(vbKeyScrollLock) = 1
    'LblNumLock.Visible = EventoTeclado.GetKeyState(vbKeyNumlock) = 1
    'LblInsert.Visible = EventoTeclado.GetKeyState(vbKeyInsert) = 1

    'If (EventoTeclado.GetKeyState(vbKeyNumlock) = 0) Then
        'CmdKey_Click (14)
    'End If

    If (EstatusMayus <> LblCapsLock.Visible) Then

        If (EventoTeclado.GetKeyState(vbKeyCapital) = 1) Then
            CmdKey(13).Picture = Teclas.ListImages(56).Picture
            CmdKey(13).BackColor = &H71CC2E
            
            CmdKey(220).Picture = Teclas.ListImages(1).Picture 'q
            CmdKey(221).Picture = Teclas.ListImages(3).Picture
            CmdKey(222).Picture = Teclas.ListImages(5).Picture
            CmdKey(223).Picture = Teclas.ListImages(7).Picture
            CmdKey(224).Picture = Teclas.ListImages(9).Picture
            CmdKey(225).Picture = Teclas.ListImages(11).Picture
            CmdKey(226).Picture = Teclas.ListImages(13).Picture
            CmdKey(227).Picture = Teclas.ListImages(15).Picture
            CmdKey(228).Picture = Teclas.ListImages(17).Picture
            CmdKey(229).Picture = Teclas.ListImages(19).Picture 'p

            CmdKey(230).Picture = Teclas.ListImages(21).Picture 'a
            CmdKey(231).Picture = Teclas.ListImages(23).Picture
            CmdKey(232).Picture = Teclas.ListImages(25).Picture
            CmdKey(233).Picture = Teclas.ListImages(27).Picture
            CmdKey(234).Picture = Teclas.ListImages(29).Picture
            CmdKey(235).Picture = Teclas.ListImages(31).Picture
            CmdKey(236).Picture = Teclas.ListImages(33).Picture
            CmdKey(237).Picture = Teclas.ListImages(35).Picture
            CmdKey(238).Picture = Teclas.ListImages(37).Picture
            CmdKey(239).Picture = Teclas.ListImages(39).Picture '�

            CmdKey(240).Picture = Teclas.ListImages(41).Picture 'z
            CmdKey(241).Picture = Teclas.ListImages(43).Picture
            CmdKey(242).Picture = Teclas.ListImages(45).Picture
            CmdKey(243).Picture = Teclas.ListImages(47).Picture
            CmdKey(244).Picture = Teclas.ListImages(49).Picture
            CmdKey(245).Picture = Teclas.ListImages(51).Picture
            CmdKey(246).Picture = Teclas.ListImages(53).Picture 'm
            
        Else
            
            CmdKey(13).Picture = Teclas.ListImages(55).Picture
            CmdKey(13).BackColor = &HE0E0E0
                   
            CmdKey(220).Picture = Teclas.ListImages(2).Picture 'q
            CmdKey(221).Picture = Teclas.ListImages(4).Picture
            CmdKey(222).Picture = Teclas.ListImages(6).Picture
            CmdKey(223).Picture = Teclas.ListImages(8).Picture
            CmdKey(224).Picture = Teclas.ListImages(10).Picture
            CmdKey(225).Picture = Teclas.ListImages(12).Picture
            CmdKey(226).Picture = Teclas.ListImages(14).Picture
            CmdKey(227).Picture = Teclas.ListImages(16).Picture
            CmdKey(228).Picture = Teclas.ListImages(18).Picture
            CmdKey(229).Picture = Teclas.ListImages(20).Picture 'p

            CmdKey(230).Picture = Teclas.ListImages(22).Picture 'a
            CmdKey(231).Picture = Teclas.ListImages(24).Picture
            CmdKey(232).Picture = Teclas.ListImages(26).Picture
            CmdKey(233).Picture = Teclas.ListImages(28).Picture
            CmdKey(234).Picture = Teclas.ListImages(30).Picture
            CmdKey(235).Picture = Teclas.ListImages(32).Picture
            CmdKey(236).Picture = Teclas.ListImages(34).Picture
            CmdKey(237).Picture = Teclas.ListImages(36).Picture
            CmdKey(238).Picture = Teclas.ListImages(38).Picture
            CmdKey(239).Picture = Teclas.ListImages(40).Picture '�

            CmdKey(240).Picture = Teclas.ListImages(42).Picture 'z
            CmdKey(241).Picture = Teclas.ListImages(44).Picture
            CmdKey(242).Picture = Teclas.ListImages(46).Picture
            CmdKey(243).Picture = Teclas.ListImages(48).Picture
            CmdKey(244).Picture = Teclas.ListImages(50).Picture
            CmdKey(245).Picture = Teclas.ListImages(52).Picture
            CmdKey(246).Picture = Teclas.ListImages(54).Picture 'm
            
        End If
        
    End If
    
    If Not (LblCapsLock.Visible = EstatusMayus) Then
         EstatusMayus = EventoTeclado.GetKeyState(vbKeyCapital)
    End If
    
End Sub

Private Sub Clear_Click()
    If (UCase(TypeName(ObjText)) = UCase("TextBox")) Then
        TxtBuffer = vbNullString
        ObjText = TxtBuffer
    ElseIf (UCase(TypeName(ObjText)) = UCase("MSFlexGrid")) Then
        Unload Me
        Send_Keys Chr(vbKeyDelete), True
    End If
End Sub

Private Sub CmdKey_Click(Index As Integer)
                                        
    If (UCase(TypeName(ObjText)) = UCase("MSFlexGrid")) Then
        
        Select Case Index
        
            Case 31 To 42
                Unload Me
                Call ProcesarFuncion(Index)
                Call EstablecerMsk(False, False, False)
                
            Case 1
                Send_Keys "{enter}"
                Unload Me
                
            Case 300 To 309
                
                If KeyboardSettings.Visible Then
                    KeyboardSettingsObject.Add CmdKey(Index).Tag, "DelayedKeys"
                End If
                
                Unload Me
                'Send_Keys CmdKey(Index).Tag
                'Send_Keys " ", True
                Exit Sub
                
        End Select
                
        Exit Sub
        
    End If
                    
    PosActText = TxtBuffer.SelStart
    
    Select Case Index
        Case 220 To 259
            If Not MskCtrl And Not MskAlt Then
                Call InsertarEnPosicion(TxtBuffer, PosActText, UCase(CmdKey(Index).Tag))
                ObjText = TxtBuffer
                Call Me.MoverAPosicion(TxtBuffer, PosActText)
            Else
                Call ProcesarTecla(CmdKey(Index).Tag)
                Unload Me
                Exit Sub
            End If
        Case 300 To 309
            'TxtBuffer.Text = TxtBuffer.Text & CmdKey(Index).Caption
            Call InsertarEnPosicion(TxtBuffer, PosActText, UCase(CmdKey(Index).Tag))
            Call EstablecerMsk
            ObjText = TxtBuffer
            Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case 405
            'TxtBuffer.Text = InsertarEnPosicion(TxtBuffer, PosActText, UCase(CmdKey(Index).Tag))
            Call InsertarEnPosicion(TxtBuffer, PosActText, UCase(CmdKey(Index).Tag))
            Call EstablecerMsk
            ObjText = TxtBuffer
            Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case 406
            Call InsertarEnPosicion(TxtBuffer, PosActText, UCase(CmdKey(Index).Tag))
            Call EstablecerMsk
            ObjText = TxtBuffer
            Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case Is = 0
            
            'If CDbl(ObjText.SelLength - ObjText.SelStart) >= 1 Then
                                            
                'On Error Resume Next
                                            
                'Dim A, B, C
                                            
                'If (ObjText.SelStart <> 0) Then
                    'A = Mid(ObjText.Text, 1, ObjText.SelStart)
                'Else
                    'A = vbNullString
                'End If
                
                'C = ObjText.SelStart + ObjText.SelLength
                
                'B = Mid(ObjText.Text, C, Len(ObjText.Text) - C)
            
                'ObjText.Text = A & B
                
                'Debug.Print ObjText.Text
                
                'ObjText.SelStart = Len(ObjText.Text)
                'ObjText.SelLength = 0
                'TxtBuffer.Text = ObjText.Text
                'TxtBuffer.SelStart = ObjText.SelStart
                'TxtBuffer.SelLength = ObjText.SelLength
                
            'Else
                
                'If TxtBuffer.Text <> "" Then TxtBuffer.Text = Mid(TxtBuffer.Text, 1, Len(TxtBuffer.Text) - 1)
                Call BorrarCaracterIzquierda(TxtBuffer, PosActText)
                Call EstablecerMsk
                ObjText = TxtBuffer
                Call Me.MoverAPosicion(TxtBuffer, PosActText)
                
            'End If
            
        Case 1
            Call EstablecerMsk
            Send_Keys "{enter}"
            Unload Me
            Exit Sub
        Case Is = 2
            'TxtBuffer.Text = TxtBuffer.Text & " "
            Call InsertarEnPosicion(TxtBuffer, PosActText, " ")
            Call EstablecerMsk
            Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case Is = 3 'ctrl
            Call EstablecerMsk(Not MskCtrl, False, True)
        Case Is = 4 'alt
            Call EstablecerMsk(False, Not MskAlt, True)
        Case Is = 7 '<-
            Call Me.MoverAPosicion(TxtBuffer, PosActText - 1, 0)
            Call EstablecerMsk
        Case Is = 8 '->
            Call Me.MoverAPosicion(TxtBuffer, PosActText + 1, 0)
            Call EstablecerMsk
        Case Is = 9 'inicio
            Call Me.MoverAPosicion(TxtBuffer, 0)
            Call EstablecerMsk
        Case Is = 10  ' fin
            Call Me.MoverAPosicion(TxtBuffer, Len(TxtBuffer))
            Call EstablecerMsk
        Case Is = 11 'insert
            Send_Keys Chr(vbKeyInsert)
            Call EstablecerMsk
            'Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case Is = 12 'supr
            Call Me.BorrarCaracterPosicion(TxtBuffer, PosActText)
            Call EstablecerMsk
            Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case Is = 13 'capslock
            Send_Keys Chr(vbKeyCapital)
            Call EstablecerMsk
            'Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case Is = 14 'numlock
            Send_Keys Chr(vbKeyNumlock)
            Call EstablecerMsk
            'Call Me.MoverAPosicion(TxtBuffer, PosActText)
        Case Is = 15 'Escape
            Unload Me
            Call EstablecerMsk
            Send_Keys "{esc}"
            Exit Sub
        Case Is = 16
            Call EstablecerMsk(False, False, Not MskShift)
        Case Is = 17
            ObjText = OriginalText
            Call EstablecerMsk
            Unload Me
            Exit Sub
        Case 31 To 42
            Unload Me
            Call ProcesarFuncion(Index)
            Call EstablecerMsk(False, False, False)
            Exit Sub
    End Select
    
End Sub

Private Sub Form_Activate()
    
    TxtBuffer.SetFocus
    
    If Not (EventoTeclado.GetKeyState(vbKeyCapital) = 1) Then
        CmdKey_Click (13)
    End If
    
End Sub

Private Sub Form_Load()
    
    FrmKeyboardPOS.Top = EventoTeclado.Top
    FrmKeyboardPOS.Left = EventoTeclado.Left
    
    MskAlt = False
    MskCtrl = False
    MskShift = False
    
    If (UCase(TypeName(ObjText)) = UCase("TextBox")) Then
        TxtBuffer = ObjText
        OriginalText = ObjText
        TxtBuffer.PasswordChar = ObjText.PasswordChar
        TxtBuffer.MaxLength = ObjText.MaxLength
        TxtBuffer.SelStart = ObjText.SelStart
        TxtBuffer.SelLength = ObjText.SelLength
        'OriginalHideSel = ObjText.HideSelection
        'ObjText.HideSelection = False
    End If
    
    If FrmKeyboardPOS.Top > 0 Then
        CambiarPosicion.Picture = Teclas.ListImages(57).Picture
    Else
        CambiarPosicion.Picture = Teclas.ListImages(58).Picture
    End If
    
    OriginalNumLockState = EventoTeclado.GetKeyState(vbKeyNumlock)
    OriginalBloqMayusState = EventoTeclado.GetKeyState(vbKeyCapital)
    OriginalInsertState = EventoTeclado.GetKeyState(vbKeyInsert)
    
    ActivarNum_Click
    
    SeleccionarTexto TxtBuffer
    
End Sub

Private Function EscribirMayuscula() As Boolean
    If EventoTeclado.GetKeyState(vbKeyCapital) = 1 Then
        'pulsada la tecla capslock
        If MskShift Then
            'pulsada la tecla shift
            MskShift = False
            EscribirMayuscula = False
        Else
            'no pulsada la tecla shift
            EscribirMayuscula = True
        End If
    Else
        'no pulsada la tecla capslock
        If MskShift Then
            'pulsada la tecla shift
            MskShift = False
            EscribirMayuscula = True
        Else
            'no pulsada la tecla shift
            EscribirMayuscula = False
        End If
    End If
End Function

Public Sub MoverAPosicion(ByRef ObjTextBox As Object, XPosicion As Integer, Optional Longitud)
        
    If XPosicion < 0 Then XPosicion = 0
    If XPosicion > Len(ObjTextBox) Then XPosicion = Len(ObjTextBox)
    
    ObjTextBox.SelStart = XPosicion
    If Not IsMissing(Longitud) Then ObjTextBox.SelLength = Longitud
    If ObjTextBox.Visible Then ObjTextBox.SetFocus
    
    PosActText = XPosicion
        
End Sub

Public Sub InsertarEnPosicion(ByRef ObjTextBox As Object, XPosicion As Integer, Caracter As String)
    
    If ObjTextBox.SelStart = 0 And ObjTextBox.SelLength = Len(ObjTextBox) Then
        ObjTextBox = vbNullString
        ObjText = ObjTextBox
    End If
    
    DoEvents
    
    If EscribirMayuscula Then
        Caracter = UCase(Caracter)
    Else
        Caracter = LCase(Caracter)
    End If
    
    If Len(ObjTextBox) = XPosicion Then
        ObjTextBox = ObjTextBox & Caracter
    Else
        If EventoTeclado.GetKeyState(vbKeyInsert) = 1 Then
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion) & Caracter & Mid(ObjTextBox, XPosicion + 2, Len(ObjTextBox))
        Else
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion) & Caracter & Mid(ObjTextBox, XPosicion + 1, Len(ObjTextBox))
        End If
    End If
    
    Call MoverAPosicion(ObjTextBox, XPosicion + 1, 0)
    
    DoEvents
    
End Sub

Public Sub BorrarCaracterIzquierda(ByRef ObjTextBox As Object, XPosicion As Integer)
    If ObjTextBox.SelStart = 0 And ObjTextBox.SelLength = Len(ObjTextBox) Then
        ObjTextBox = vbNullString
        ObjText = ObjTextBox
    End If
    If Len(ObjTextBox) > 0 And XPosicion > 0 Then
        ObjTextBox = Mid(ObjTextBox, 1, XPosicion - 1) & Mid(ObjTextBox, XPosicion + 1, Len(ObjTextBox))
        Call MoverAPosicion(ObjTextBox, XPosicion - 1, 0)
    End If
End Sub

Public Sub BorrarCaracterPosicion(ByRef ObjTextBox As Object, XPosicion As Integer)
    If ObjTextBox.SelStart = 0 And ObjTextBox.SelLength = Len(ObjTextBox) Then
        ObjTextBox = vbNullString
        ObjText = ObjTextBox
    End If
    If Len(ObjTextBox) > 0 And XPosicion < Len(ObjTextBox) Then
        If XPosicion > 1 Then
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion - 1) & Mid(ObjTextBox, XPosicion + 2, Len(ObjTextBox))
            Call MoverAPosicion(ObjTextBox, XPosicion - 1, 0)
        Else
            ObjTextBox = Mid(ObjTextBox, 2, Len(ObjTextBox))
            Call MoverAPosicion(ObjTextBox, 0, 0)
        End If
    End If
End Sub

Public Sub ProcesarTecla(Caracter As String)
    If MskCtrl Then
        Send_Keys Chr(vbKeyControl) & Caracter, False, False
        MskCtrl = False
    ElseIf MskAlt Then
        Send_Keys "{%}" & Caracter, False, False
        MskAlt = False
    End If
End Sub

Public Sub EstablecerMsk(Optional MCtrl As Boolean = False, Optional MAlt As Boolean = False, Optional MShift As Boolean = False)
    MskCtrl = MCtrl
    MskAlt = MAlt
    MskShift = MShift
End Sub

Public Sub ProcesarFuncion(Tecla As Integer)
    
    DoEvents
    
    Dim EnviarTeclado As String
    
    If MskCtrl Or MskAlt Then
        If MskCtrl Then
            EnviarTeclado = "{^}"
        ElseIf MskAlt Then
            EnviarTeclado = "{%}"
        Else
            EnviarTeclado = ""
        End If
        
        Call EstablecerMsk(False, False, False)
    End If
    
    DoEvents
    
    Select Case Tecla
        Case 31 'F1
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF1)
        Case 32 'F2
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF2)
        Case 33 'F3
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF3)
        Case 34 'F4
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF4)
        Case 35 'F5
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF5)
        Case 36 'F6
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF6)
        Case 37 'F7
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF7)
        Case 38 'F8
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF8)
        Case 39 'F9
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF9)
        Case 40 'F10
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF10)
        Case 41 'F11
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF11)
        Case 42 'F12
            EnviarTeclado = EnviarTeclado & Chr(vbKeyF12)
    End Select
    
    Send_Keys EnviarTeclado, True
    
    DoEvents
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    ActLeds.Enabled = False
    
    If (UCase(TypeName(ObjText)) = UCase("TextBox")) Then
        ObjText.SelStart = TxtBuffer.SelStart
        ObjText.SelLength = TxtBuffer.SelLength
        'ObjText.HideSelection = OriginalHideSel
    End If
        
    If EventoTeclado.GetKeyState(vbKeyNumlock) <> OriginalNumLockState Then ClaseRutinas.SendKeys Chr(vbKeyNumlock), True
    If EventoTeclado.GetKeyState(vbKeyCapital) <> OriginalBloqMayusState Then ClaseRutinas.SendKeys Chr(vbKeyCapital), True
    If EventoTeclado.GetKeyState(vbKeyInsert) <> OriginalInsertState Then ClaseRutinas.SendKeys Chr(vbKeyInsert), True
    
End Sub

Private Sub KeyboardSettings_Click()
    ConfigurarTeclado KeyboardSettingsObject
End Sub

Private Sub lblEscape_Click()
    CmdKey_Click 15
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Public Function GetTaskBarHeight() As Long
    
    Dim ABD As APPBARDATA

    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarHeight = ABD.rc.Bottom - ABD.rc.Top

End Function
