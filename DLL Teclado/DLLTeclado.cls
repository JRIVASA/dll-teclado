VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DLLTeclado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Const ABM_GETTASKBARPOS = &H5

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hWnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" (ByVal dwMessage As Long, pData As APPBARDATA) As Long

Private Declare Function GetDeviceCaps Lib "gdi32" _
        (ByVal hdc As Long, ByVal nIndex As Long) As Long

Const HorizontalResolution = 8
Const VerticalResolution = 10

Public Sub ShowKeyboard(TxtObj As Object, _
Optional ControlTop, _
Optional FormTop, _
Optional ByRef KeyboardSettings)
    
    If TxtObj Is Nothing Then
        Exit Sub
    End If
    
    Set ObjText = TxtObj
    
    If Not (IsMissing(ControlTop) Or IsMissing(FormTop)) Then
        If ((ControlTop + FormTop) > (Screen.Height / 2)) Then
            EventoTeclado.Top = Screen.Height - FrmKeyboard.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
            'EventoTeclado.Top = Screen.Height - FrmKeyboard.Height
        Else
            EventoTeclado.Top = 0
        End If
    Else
        EventoTeclado.Top = Screen.Height - FrmKeyboard.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        If EventoTeclado.Top < 0 Then EventoTeclado.Top = Screen.Height - FrmKeyboard.Height
        'EventoTeclado.Top = Screen.Height - FrmKeyboard.Height
    End If
    
    EventoTeclado.Left = (Screen.Width / 2) - (FrmKeyboard.Width / 2)
    
    FrmKeyboard.Left = EventoTeclado.Left
    FrmKeyboard.Top = EventoTeclado.Top
    
    If Not IsMissing(KeyboardSettings) Then
        If Not KeyboardSettings Is Nothing Then
            
            FrmKeyboard.KeyboardSettings.Visible = True
            Set FrmKeyboard.KeyboardSettingsObject = KeyboardSettings
            
            Select Case Val(KeyboardSettings("PosicionInicial"))
                Case 1
                    ' El teclado ya esta anclado en el fondo de la pantalla.
                Case 2
                    ' Anclar al Top de la pantalla.
                    EventoTeclado.Top = 0
                    FrmKeyboard.Top = EventoTeclado.Top
            End Select
            
        End If
    End If
    
    FrmKeyboard.Show vbModal
    
    If Not IsMissing(KeyboardSettings) Then
        If Not KeyboardSettings Is Nothing Then
            Set KeyboardSettings = FrmKeyboard.KeyboardSettingsObject
        End If
    End If
    
End Sub

Public Sub ShowKeyboardPOS(ByRef TxtObj As Object)
    
    If TxtObj Is Nothing Then
        Exit Sub
    End If
    
    Set ObjText = TxtObj
    
    Dim ObjTop, ObjLeft, TskH
    
    ObjTop = Control_TopReal(TxtObj)
    'ObjLeft = Control_LeftReal(TxtObj)
    
    TskH = (GetTaskBarHeight * Screen.TwipsPerPixelY)
    If TskH = Screen.Height Then TskH = 0
    
    ' Si el objeto esta en la parte de arriba, lo mostramos abajo para que no lo tape.
    If Screen.Height - TskH - FrmKeyboardPOS.Height > (ObjTop + ObjText.Height) Then
        EventoTeclado.Top = Screen.Height - FrmKeyboardPOS.Height - TskH
        If EventoTeclado.Top < 0 Then EventoTeclado.Top = Screen.Height - FrmKeyboardPOS.Height
    Else
        ' Sino el objeto esta abajo, anclarlo el teclado hacia arriba.
        EventoTeclado.Top = 0
    End If
    
    EventoTeclado.Left = (Screen.Width / 2) - (FrmKeyboardPOS.Width / 2)
    If EventoTeclado.Left < 0 Then EventoTeclado.Left = 0
    
    FrmKeyboardPOS.Left = EventoTeclado.Left
    FrmKeyboardPOS.Top = EventoTeclado.Top
    
    FrmKeyboardPOS.Show vbModal
    
    Set FrmKeyboardPOS = Nothing
    
End Sub

Private Sub Class_Initialize()
    EventoTeclado.Left = 0
    EventoTeclado.Top = 0
End Sub

Public Sub SetCoordenaas(CLeft As Integer, CTop As Integer)
    EventoTeclado.Left = CLeft
    EventoTeclado.Top = CTop
End Sub

Public Sub SendKeys(Caracteres As String, Optional opcion As Boolean = True, Optional Repetir As Boolean = False)
    Send_Keys Caracteres, opcion, Repetir
End Sub

Public Function EstadoTeclas(Tecla As Integer) As Boolean
    EstadoTeclas = EventoTeclado.GetKeyState(Tecla) = 1
End Function

Public Function ShowNumPad(ByRef Valor As Double, Optional ByVal Decimales As Integer = 2, Optional ModoAlfanumerico As Boolean = False) As Variant
    
    If Not ModoAlfanumerico Then
        FrmNumPad.CantDec = Decimales
        FrmNumPad.ValorOriginal = Valor
    Else
        FrmNumPad.ModoAlfanumerico = True 'Obtener los d�gitos tal y como el usuario los escribi�.
    End If
    
    FrmNumPad.Show vbModal
    
    If Not ModoAlfanumerico Then
        ShowNumPad = FormatNumber(FrmNumPad.ValorNumerico, Decimales)
        Valor = CDbl(ShowNumPad)
    Else
        ShowNumPad = FrmNumPad.ValorAlfanumerico
        
        If IsNumeric(ShowNumPad) Then
            Valor = CDbl(ShowNumPad)
        Else
            Valor = Val(ShowNumPad)
        End If
    End If
    
    Set FrmNumPad = Nothing
    
End Function

Public Function ShowNumPadV2(ByVal ValorOriginal As Variant, _
Optional ByVal Decimales As Integer = 2, _
Optional ByVal ModoAlfanumerico As Boolean = False, _
Optional ByRef OutValorNumerico As Double = 0, _
Optional ByRef TextObjForVisibility As Object = Nothing, _
Optional ByVal ModoEscrituraDirecta = False _
) As Variant
    
    If Not ModoAlfanumerico Then
        FrmNumPadV2.CantDec = Decimales
        If Not IsNumeric(ValorOriginal) Then ValorOriginal = Val(ValorOriginal)
        FrmNumPadV2.ValorOriginal = CDbl(ValorOriginal)
    Else
        FrmNumPadV2.ModoAlfanumerico = True 'Obtener los d�gitos tal y como el usuario los escribi�.
        FrmNumPadV2.ValorOriginal = CStr(ValorOriginal)
    End If
    
    If ModoEscrituraDirecta Then
        Set FrmNumPadV2.ObjText = TextObjForVisibility
        FrmNumPadV2.ModoEscrituraDirecta = True
    End If
    
    Dim Obj, ObjTop, ObjLeft, ScrW, ScrH, TskH, TskW, RemH, RemW, Placed As Boolean
    
    If Not TextObjForVisibility Is Nothing Then
        
        Set Obj = TextObjForVisibility
        ObjTop = Control_TopReal(Obj)
        ObjLeft = Control_LeftReal(Obj)
        ScrW = Screen.Width
        ScrH = Screen.Height
        TskH = (GetTaskBarHeight * Screen.TwipsPerPixelY)
        If TskH = ScrH Then TskH = 0
        TskW = (GetTaskBarWidth * Screen.TwipsPerPixelX)
        If TskW = ScrW Then TskW = 0
        
        If FrmNumPadV2.Width + (ObjLeft + Obj.Width) <= ScrW - TskW Then
            
            ' Intentar primero colocarlo a la derecha del Objeto
            
            If ((ObjLeft + Obj.Width) * 1.1 + FrmNumPadV2.Width) > (ScrW - TskH) Then
                FrmNumPadV2.Left = (ObjLeft + Obj.Width)
            Else
                FrmNumPadV2.Left = (ObjLeft + Obj.Width) * 1.1
            End If
            
            If FrmNumPadV2.Height > Obj.Height Then
                FrmNumPadV2.Top = (ObjTop - ((FrmNumPadV2.Height - Obj.Height) / 2))
            Else
                FrmNumPadV2.Top = (ObjTop + ((Obj.Height - FrmNumPadV2.Height) / 2))
            End If
            
            If (FrmNumPadV2.Top + FrmNumPadV2.Height) > ScrH - TskH Then FrmNumPadV2.Top = ScrH - TskH - FrmNumPadV2.Height
            
        ElseIf TskW + FrmNumPadV2.Width < ObjLeft Then
            
            ' Ver si entonces hay espacio a la izquierda.
            
            If ((ObjLeft / 1.05) >= (TskW + FrmNumPadV2.Width)) Then
                FrmNumPadV2.Left = (ObjLeft / 1.05) - FrmNumPadV2.Width
            Else
                FrmNumPadV2.Left = (ObjLeft - FrmNumPadV2.Width)
            End If
            
            If FrmNumPadV2.Height > Obj.Height Then
                FrmNumPadV2.Top = (ObjTop - ((FrmNumPadV2.Height - Obj.Height) / 2))
            Else
                FrmNumPadV2.Top = (ObjTop + ((Obj.Height - FrmNumPadV2.Height) / 2))
            End If
            
            If (FrmNumPadV2.Top + FrmNumPadV2.Height) > ScrH - TskH Then FrmNumPadV2.Top = ScrH - TskH - FrmNumPadV2.Height
            
        ElseIf FrmNumPadV2.Height + (ObjTop + Obj.Height) <= ScrH - TskH Then
            
            ' Sino intentar colocarlo por debajo del objeto.
            
            If ((ObjTop + Obj.Height) * 1.1 + FrmNumPadV2.Height) > (ScrH - TskH) Then
                FrmNumPadV2.Top = (ObjTop + Obj.Height)
            Else
                FrmNumPadV2.Top = (ObjTop + Obj.Height) * 1.1
            End If
            
            If FrmNumPadV2.Width > Obj.Width Then
                FrmNumPadV2.Left = (ObjLeft - ((FrmNumPadV2.Width - Obj.Width) / 2))
            Else
                FrmNumPadV2.Left = (ObjLeft + ((Obj.Width - FrmNumPadV2.Width) / 2))
            End If
            
            If (FrmNumPadV2.Left + FrmNumPadV2.Width) > ScrW - TskW Then FrmNumPadV2.Left = ScrW - TskW - FrmNumPadV2.Width
            
        Else
            
            ' Entonces sea como sea lo va a tapar... lo centramos entonces
            
            Call AjustarPantalla(FrmNumPadV2)
            
        End If
        
    Else
        Call AjustarPantalla(FrmNumPadV2)
    End If
    
    If FrmNumPadV2.Left < 0 Then FrmNumPadV2.Left = 0
    If FrmNumPadV2.Top < 0 Then FrmNumPadV2.Top = 0
    
    FrmNumPadV2.Show vbModal
    
    If Not ModoAlfanumerico Then
        OutValorNumerico = FrmNumPadV2.ValorNumerico
        ShowNumPadV2 = FormatNumber(OutValorNumerico, Decimales)
    Else
        ShowNumPadV2 = FrmNumPadV2.ValorAlfanumerico
        
        If IsNumeric(ShowNumPadV2) Then
            OutValorNumerico = CDbl(ShowNumPadV2)
        Else
            OutValorNumerico = Val(ShowNumPadV2)
        End If
    End If
    
    Set FrmNumPadV2 = Nothing
    Set FrmKeyboard = Nothing
    
End Function
