VERSION 5.00
Begin VB.Form FrmNumPadV2 
   Appearance      =   0  'Flat
   BackColor       =   &H00404040&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5745
   ClientLeft      =   -45
   ClientTop       =   -45
   ClientWidth     =   4350
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5745
   ScaleWidth      =   4350
   Begin VB.Timer KeyBdStatus 
      Enabled         =   0   'False
      Interval        =   150
      Left            =   0
      Top             =   360
   End
   Begin VB.Frame FrmKeyPad 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   5745
      Left            =   15
      TabIndex        =   0
      Top             =   0
      Width           =   4365
      Begin VB.TextBox TxtNumero 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         HideSelection   =   0   'False
         Left            =   90
         TabIndex        =   15
         Top             =   480
         Width           =   4150
      End
      Begin VB.CommandButton Cmd_Enter 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Enter"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3705
         Left            =   3240
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1950
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "0"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   0
         Left            =   90
         Picture         =   "FrmNumPadV2.frx":0000
         TabIndex        =   13
         Top             =   4740
         Width           =   2070
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "1"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   1
         Left            =   90
         Picture         =   "FrmNumPadV2.frx":08CA
         TabIndex        =   12
         Top             =   3810
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "2"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   2
         Left            =   1140
         Picture         =   "FrmNumPadV2.frx":1194
         TabIndex        =   11
         Top             =   3810
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "3"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   3
         Left            =   2190
         Picture         =   "FrmNumPadV2.frx":1A5E
         TabIndex        =   10
         Top             =   3810
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "4"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   4
         Left            =   90
         Picture         =   "FrmNumPadV2.frx":2328
         TabIndex        =   9
         Top             =   2880
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "5"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   5
         Left            =   1140
         Picture         =   "FrmNumPadV2.frx":2BF2
         TabIndex        =   8
         Top             =   2880
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "6"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   6
         Left            =   2190
         Picture         =   "FrmNumPadV2.frx":34BC
         TabIndex        =   7
         Top             =   2880
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "7"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   7
         Left            =   90
         Picture         =   "FrmNumPadV2.frx":3D86
         TabIndex        =   6
         Top             =   1950
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "8"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   8
         Left            =   1140
         Picture         =   "FrmNumPadV2.frx":4650
         TabIndex        =   5
         Top             =   1950
         Width           =   1020
      End
      Begin VB.CommandButton Numeros 
         BackColor       =   &H00E0E0E0&
         Caption         =   "9"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   9
         Left            =   2190
         Picture         =   "FrmNumPadV2.frx":4F1A
         TabIndex        =   4
         Top             =   1950
         Width           =   1020
      End
      Begin VB.CommandButton Cmd_Decimal 
         BackColor       =   &H00E0E0E0&
         Caption         =   "."
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Left            =   2190
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   4740
         Width           =   1020
      End
      Begin VB.CommandButton Cmd_Back 
         BackColor       =   &H00E0E0E0&
         Caption         =   "<---"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Left            =   2190
         Picture         =   "FrmNumPadV2.frx":57E4
         TabIndex        =   2
         Top             =   1020
         Width           =   2070
      End
      Begin VB.CommandButton Cmd_ClearAll 
         BackColor       =   &H00E0E0E0&
         Caption         =   "CLS"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Left            =   120
         TabIndex        =   1
         Top             =   1020
         Width           =   2040
      End
      Begin VB.Shape NumLockLed 
         BackColor       =   &H00FFFFFF&
         BorderColor     =   &H00FFFFFF&
         FillColor       =   &H00404040&
         FillStyle       =   0  'Solid
         Height          =   180
         Left            =   585
         Shape           =   3  'Circle
         Top             =   165
         Visible         =   0   'False
         Width           =   150
      End
      Begin VB.Label LblTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   120
         Width           =   3375
      End
      Begin VB.Image Salir 
         Height          =   480
         Left            =   3750
         Picture         =   "FrmNumPadV2.frx":5AEE
         Top             =   0
         Width           =   480
      End
      Begin VB.Shape InsertLed 
         BackColor       =   &H00FFFFFF&
         BorderColor     =   &H00FFFFFF&
         FillStyle       =   0  'Solid
         Height          =   180
         Left            =   885
         Shape           =   3  'Circle
         Top             =   165
         Visible         =   0   'False
         Width           =   150
      End
   End
End
Attribute VB_Name = "FrmNumPadV2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ModoAlfanumerico As Boolean
Public ModoEscrituraDirecta As Boolean
Public ValorNumerico As Double
Public ValorAlfanumerico As String
Public CantDec As Integer

Public PosActText As Integer
Public ObjText As Object

Private mValorOriginal As Variant
Private EventoProgramado As Boolean

Private ClaseRutinas As New cls_Rutinas

Property Get ValorOriginal()
    ValorOriginal = mValorOriginal
End Property

Property Let ValorOriginal(pValor As Variant)
    mValorOriginal = pValor
End Property

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub FrmKeyPad_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Form_MouseMove Button, Shift, X, Y
End Sub

Private Sub LblTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Form_MouseMove Button, Shift, X, Y
End Sub

Private Sub Cmd_Back_Click()
    
    DoEvents
    
    If ModoEscrituraDirecta Then
        
        Call BorrarCaracterIzquierda(TxtNumero, PosActText)
        ObjText.Text = TxtNumero
        Call MoverAPosicion(TxtNumero, PosActText)
        
    Else
        
        Dim PosAct As Integer
        
        PosAct = Me.TxtNumero.SelStart
        
        If Me.TxtNumero.SelLength > 0 Then
            Me.TxtNumero.Text = vbNullString
        Else
            If Me.TxtNumero.SelStart > 0 Then
                EventoProgramado = True
                Me.TxtNumero.Text = Mid(Me.TxtNumero.Text, 1, Me.TxtNumero.SelStart - 1) & Mid(Me.TxtNumero.Text, Me.TxtNumero.SelStart + 1)
                If Me.TxtNumero.Text = vbNullString Then
                    TxtNumero_Change
                Else
                    PosAct = PosAct - 1
                    Me.TxtNumero.SelStart = PosAct
                End If
            End If
        End If
        
    End If
    
    DoEvents
    
End Sub

Private Sub Cmd_ClearAll_Click()
    DoEvents
    Me.TxtNumero.Text = ""
    If ModoEscrituraDirecta Then ObjText.Text = TxtNumero.Text
    DoEvents
End Sub

Private Sub Cmd_Decimal_Click()
    
    DoEvents
    
    Dim PosAct As Integer, CarDecimal As String
    
    CarDecimal = Me.Cmd_Decimal.Caption
    
    If ModoEscrituraDirecta Then
        Call InsertarEnPosicion(TxtNumero, PosActText, CarDecimal)
        ObjText.Text = TxtNumero.Text
        Call MoverAPosicion(TxtNumero, PosActText)
    Else
        
        PosAct = Me.TxtNumero.SelStart
        
        If InStr(1, Me.TxtNumero.Text, Me.Cmd_Decimal.Caption) <= 0 Then
            TxtNumero.Text = Mid(Me.TxtNumero.Text, 1, PosAct) & CarDecimal & Mid(Me.TxtNumero.Text, PosAct + 2)
            TxtNumero.SelStart = PosAct + 1
        End If
        
    End If
    
    DoEvents
    
End Sub

Private Sub Cmd_Enter_Click()
    
    On Error Resume Next
    
    If ModoEscrituraDirecta Then
        
        Send_Keys "{enter}"
        Unload Me
        Exit Sub
    
    Else
        
        If Not ModoAlfanumerico And Trim(Me.TxtNumero.Text) = "" Then
            Me.TxtNumero.SetFocus
            Exit Sub
        End If
        
        ValorAlfanumerico = Me.TxtNumero.Text
        
        If IsNumeric(ValorAlfanumerico) Then
            ValorNumerico = CDbl(ValorAlfanumerico)
        Else
            ValorNumerico = Val(ValorAlfanumerico)
        End If
        
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Activate()
    Cmd_Decimal.Caption = ClaseRutinas.SDecimal
    
    SeleccionarTexto TxtNumero
    
    If TxtNumero.Visible Then TxtNumero.SetFocus
End Sub

Private Sub Form_Load()
    
    InsertLed.Visible = False
    NumLockLed.Visible = False
    
    If ModoEscrituraDirecta Then
        'TxtNumero.Visible = False
        
        If (UCase(TypeName(ObjText)) = UCase("TextBox")) Then
            TxtNumero.Text = ObjText.Text
            mValorOriginal = ObjText.Text
            TxtNumero.PasswordChar = ObjText.PasswordChar
            TxtNumero.MaxLength = ObjText.MaxLength
            TxtNumero.SelStart = ObjText.SelStart
            TxtNumero.SelLength = ObjText.SelLength
        End If
    Else
        If Not IsEmpty(mValorOriginal) Then
            If ModoAlfanumerico Then
                TxtNumero.Text = FormatNumber(mValorOriginal, CantDec)
            Else
                TxtNumero.Text = mValorOriginal
            End If
            SeleccionarTexto TxtNumero
        End If
    End If
    
End Sub

Private Sub KeyBdStatus_Timer()
    
    Dim Kbd As New DLLTeclado
    
    NumLockLed.Visible = Kbd.EstadoTeclas(vbKeyNumlock)
    InsertLed.Visible = Kbd.EstadoTeclas(vbKeyInsert)
    
End Sub

Private Sub Numeros_Click(Index As Integer)
    
    DoEvents
    
    If ModoEscrituraDirecta Then
        Call InsertarEnPosicion(TxtNumero, PosActText, CStr(Index))
        ObjText.Text = TxtNumero.Text
        Call MoverAPosicion(TxtNumero, PosActText)
    Else
        
        Dim PosAct As Integer
        
        If TxtNumero.SelLength = Len(TxtNumero) Then
            EventoProgramado = True
            TxtNumero.Text = vbNullString
        End If
        
        PosAct = Me.TxtNumero.SelStart
        
        TxtNumero.Text = Mid(Me.TxtNumero.Text, 1, PosAct) & Index & Mid(Me.TxtNumero.Text, PosAct + 1)
        TxtNumero.SelStart = PosAct + 1
        
    End If
    
    DoEvents
    
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub TxtNumero_Change()
    If Not EventoProgramado Then
        If Not ModoAlfanumerico Then
            If IsNumeric(TxtNumero.Text) Then
                If CDbl(TxtNumero.Text) < 0 Then
                    TxtNumero.Text = "0.00"
                    TxtNumero_GotFocus
                End If
            Else
                TxtNumero.Text = "0.00"
                TxtNumero_GotFocus
            End If
        End If
    Else
        EventoProgramado = False ' Ignorar
    End If
End Sub

Private Sub TxtNumero_GotFocus()
    SeleccionarTexto TxtNumero
End Sub

Private Sub TxtNumero_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Cmd_Enter_Click
    Else
        KeyAscii = 0
    End If
End Sub

Public Sub MoverAPosicion(ByRef ObjTextBox As Object, XPosicion As Integer, Optional Longitud)
        
    If XPosicion < 0 Then XPosicion = 0
    If XPosicion > Len(ObjTextBox) Then XPosicion = Len(ObjTextBox)
    
    ObjTextBox.SelStart = XPosicion
    If Not IsMissing(Longitud) Then ObjTextBox.SelLength = Longitud
    If ObjTextBox.Visible Then ObjTextBox.SetFocus
    
    PosActText = XPosicion
        
End Sub

Public Sub InsertarEnPosicion(ByRef ObjTextBox As Object, XPosicion As Integer, Caracter As String)
    
    DoEvents
    
    If EscribirMayuscula Then
        Caracter = UCase(Caracter)
    Else
        Caracter = LCase(Caracter)
    End If
    
    If Len(ObjTextBox) = XPosicion Then
        ObjTextBox = ObjTextBox & Caracter
    Else
        If EventoTeclado.GetKeyState(vbKeyInsert) = 1 Then
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion) & Caracter & Mid(ObjTextBox, XPosicion + 1, Len(ObjTextBox))
        Else
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion) & Caracter & Mid(ObjTextBox, XPosicion + 2, Len(ObjTextBox))
        End If
    End If
    
    Call MoverAPosicion(ObjTextBox, XPosicion + 1, 0)
    
    DoEvents
    
End Sub

Public Sub BorrarCaracterIzquierda(ByRef ObjTextBox As Object, XPosicion As Integer)
    If Len(ObjTextBox) > 0 And XPosicion > 0 Then
        ObjTextBox = Mid(ObjTextBox, 1, XPosicion - 1) & Mid(ObjTextBox, XPosicion + 1, Len(ObjTextBox))
        Call MoverAPosicion(ObjTextBox, XPosicion - 1, 0)
    End If
End Sub

Public Sub BorrarCaracterPosicion(ByRef ObjTextBox As Object, XPosicion As Integer)
    If Len(ObjTextBox) > 0 And XPosicion < Len(ObjTextBox) Then
        If XPosicion > 1 Then
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion - 1) & Mid(ObjTextBox, XPosicion + 2, Len(ObjTextBox))
            Call MoverAPosicion(ObjTextBox, XPosicion - 1, 0)
        Else
            ObjTextBox = Mid(ObjTextBox, 2, Len(ObjTextBox))
            Call MoverAPosicion(ObjTextBox, 0, 0)
        End If
    End If
End Sub
