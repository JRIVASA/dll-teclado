Attribute VB_Name = "EventoTeclado"
Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Private Enum KeyConst
    KEYEVENTF_EXTENDEDKEY = &H1
    KEYEVENTF_KEYUP = &H2
    KF_ALTDOWN = &H2000
    KF_DLGMODE = &H800
    KF_EXTENDED = &H100
    KF_MENUMODE = &H1000
    KF_REPEAT = &H4000
    KF_UP = &H8000
    KF_NORMAL = &H0
End Enum

Private Const ABM_GETTASKBARPOS = &H5

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hWnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" (ByVal dwMessage As Long, pData As APPBARDATA) As Long

Private Declare Function GetDeviceCaps Lib "gdi32" _
        (ByVal hdc As Long, ByVal nIndex As Long) As Long

Public Declare Sub ReleaseCapture Lib "user32" ()

Private Declare Function SendMessage Lib "user32.dll" _
Alias "SendMessageA" ( _
ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
ByVal lParam As Any) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpString _
    As Any, ByVal lpFileName As String) As Long

Public EstatusMayus As Boolean
Public Left As Integer
Public Top As Integer
Public ObjText As Object

Public Sub Send_Keys(Caracteres As String, Optional opcion As Boolean = True, Optional Repetir As Boolean = False)
    
    'SOBRECARGAMOS LA FUNCI�N SENDKEYS ESCRIBIENDO UNA PARECIDA PERO USANDO:
    'Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
    'Y SUS CONSTANTES PARA EMULAR ESTA.
    
    'POR: EDGAR IV�N S�NCHEZ MORA
    'PARA: PowerPuff Software Inc.
    
    Dim CCont As Integer, CtrlPend As Boolean, AltPend As Boolean, ShiftPend As Boolean
    Dim SubCommand As String
    
    'Caracteres = EjecutarAccion(Caracteres)
    CtrlPend = False
    AltPend = False
    ShiftPend = False
    
    Do While Len(Caracteres) > 0
        If Mid(Caracteres, 1, 1) = "{" And InStr(2, Caracteres, "}") > 0 Then
            'KEYEVENTF_EXTENDEKEY
            'dwflags = 0 => la tecla se desactiva al pulsarse la misma tecla
            'dwflags = 1 => la tecla se desactiva con la tecla contraria
            'dwflags =  2 => nothing
            'dwExtraInfo = 1 => 0 nothing
            'dwExtraInfo = 2 =>
            SubCommand = ExtraerIntervalo(Caracteres, True)
            Select Case SubCommand
                Case "{^}"
                    If CtrlPend Then
                        CtrlPend = False
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    Else
                        CtrlPend = True
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                    End If
                Case "{%}"
                    If AltPend Then
                        AltPend = False
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_UP, 0)
                    Else
                        AltPend = True
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                    End If
                Case "{+}"
                    If ShiftPend Then
                        ShiftPend = False
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    Else
                        ShiftPend = True
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                    End If
                Case "{WINDOWS}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{MENU}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{SCROLLLOCK}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{PAUSE}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{INSERT}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
'                Case "{BACKSPACE}", "{BS}", "{BKSP}", "{DELETE}", "{DEL}"
'                    Call keybd_event(ExecAction(SubCommand), IIf(Opcion, 1, 0), KeyConst.KF_REPEAT, 0)
                Case Else
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    'Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
            End Select
        Else
            If Mid(Caracteres, 1, 1) = "~" Then
                Call keybd_event(vbKeyReturn, IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                'Call keybd_event(vbKeyReturn, IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Caracteres = Mid(Caracteres, 2, Len(Caracteres))
            Else
                If NecesitaDoble(CByte(Asc(Mid(Caracteres, 1, 1)))) Then
                    If CByte(Asc(Mid(Caracteres, 1, 1))) = 18 Then
                        If AltPend Then
                            AltPend = False
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_UP, 0)
                        Else
                            AltPend = True
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_ALTDOWN, 0)
                        End If
                    ElseIf CByte(Asc(Mid(Caracteres, 1, 1))) = vbKeyControl Then
                        If CtrlPend Then
                            CtrlPend = False
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        Else
                            CtrlPend = True
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                        End If
                    ElseIf CByte(Asc(Mid(Caracteres, 1, 1))) = vbKeyShift Then
                        If ShiftPend Then
                            ShiftPend = False
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        Else
                            ShiftPend = True
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                        End If
                    Else
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        If CtrlPend Then
                            CtrlPend = False
                            Call keybd_event(CByte(vbKeyControl), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        End If
                        If ShiftPend Then
                            ShiftPend = False
                            Call keybd_event(CByte(vbKeyShift), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        End If
                        If AltPend Then
                            AltPend = False
                            Call keybd_event(CByte(18), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        End If
                    End If
                    Caracteres = Mid(Caracteres, 2, Len(Caracteres))
                Else
                    If Repetir Then
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_REPEAT, 0)
                    Else
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                        'Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                    Caracteres = Mid(Caracteres, 2, Len(Caracteres))
                    If CtrlPend Then
                        CtrlPend = False
                        Call keybd_event(CByte(vbKeyControl), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                    If ShiftPend Then
                        ShiftPend = False
                        Call keybd_event(CByte(vbKeyShift), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                    If AltPend Then
                        AltPend = False
                        Call keybd_event(CByte(18), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                End If
            End If
        End If
    Loop
    
    If CtrlPend Then
        CtrlPend = False
        Call keybd_event(CByte(vbKeyControl), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
    End If
    
    If ShiftPend Then
        ShiftPend = False
        Call keybd_event(CByte(vbKeyShift), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
    End If
    
    If AltPend Then
        AltPend = False
        Call keybd_event(CByte(18), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
    End If
    
End Sub

Private Function ExecAction(ByRef Accion As String) As Byte
    Select Case UCase(Accion)
        Case "{BACKSPACE}", "{BS}", "{BKSP}"
            ExecAction = CByte(vbKeyBack)
        Case "{SPACE}"
            ExecAction = CByte(vbKeySpace)
        Case "{BREAK}"
            ExecAction = CByte(vbKeyCancel)
        Case "{PAUSE}"
            ExecAction = CByte(vbKeyPause)
        Case "{CAPSLOCK}"
            ExecAction = CByte(vbKeyCapital)
        Case "{DELETE}", "{DEL}"
            ExecAction = CByte(vbKeyDelete)
        Case "{DOWN}"
            ExecAction = CByte(vbKeyDown)
        Case "{END}"
            ExecAction = CByte(vbKeyEnd)
        Case "{ENTER}", "~"
            ExecAction = CByte(vbKeyReturn)
        Case "{ESC}"
            ExecAction = CByte(vbKeyEscape)
        Case "{HELP}"
            ExecAction = CByte(vbKeyHelp)
        Case "{HOME}"
            ExecAction = CByte(vbKeyHome)
        Case "{INSERT}", "{INS}"
            ExecAction = CByte(vbKeyInsert)
        Case "{LEFT}"
            ExecAction = CByte(vbKeyLeft)
        Case "{NUMLOCK}"
            ExecAction = CByte(vbKeyNumlock)
        Case "{PGDN}"
            ExecAction = CByte(vbKeyPageDown)
        Case "{PGUP}"
            ExecAction = CByte(vbKeyPageUp)
        Case "{PRTSC}"
            ExecAction = CByte(vbKeyPrint)
        Case "{RIGHT}"
            ExecAction = CByte(vbKeyRight)
        Case "{SCROLLLOCK}"
            ExecAction = CByte(145)
        Case "{TAB}"
            ExecAction = CByte(vbKeyTab)
        Case "{UP}"
            ExecAction = CByte(vbKeyUp)
        Case "{F1}"
            ExecAction = CByte(vbKeyF1)
        Case "{F2}"
            ExecAction = CByte(vbKeyF2)
        Case "{F3}"
            ExecAction = CByte(vbKeyF3)
        Case "{F4}"
            ExecAction = CByte(vbKeyF4)
        Case "{F5}"
            ExecAction = CByte(vbKeyF5)
        Case "{F6}"
            ExecAction = CByte(vbKeyF6)
        Case "{F7}"
            ExecAction = CByte(vbKeyF7)
        Case "{F8}"
            ExecAction = CByte(vbKeyF8)
        Case "{F9}"
            ExecAction = CByte(vbKeyF9)
        Case "{F10}"
            ExecAction = CByte(vbKeyF10)
        Case "{F11}"
            ExecAction = CByte(vbKeyF11)
        Case "{F12}"
            ExecAction = CByte(vbKeyF12)
        Case "{F13}"
            ExecAction = CByte(vbKeyF13)
        Case "{F14}"
            ExecAction = CByte(vbKeyF14)
        Case "{F15}"
            ExecAction = CByte(vbKeyF15)
        Case "{F16}"
            ExecAction = CByte(vbKeyF16)
        Case "{+}"
            ExecAction = CByte(vbKeyShift)
        Case "{^}"
            ExecAction = CByte(vbKeyControl)
        Case "{%}"
            ExecAction = CByte(18)
        Case "{WINDOWS}"
            ExecAction = CByte(91)
        Case "{MENU}"
            ExecAction = CByte(93)
    End Select
End Function

Private Function ExtraerIntervalo(ByRef Cadena As String, Optional Eliminar As Boolean = False) As String
    
    Dim ILLave As Integer, FLlave As Integer, SubCaracter As String, Caracteres As String
    
    Const ValidCad = "{BACKSPACE}{BS}{BKSP}{BREAK}{CAPSLOCK}{DELETE}{DEL}{DOWN}{END}{ENTER}~{ESC}{HELP}{HOME}{INSERT}{INS}{LEFT}{NUMLOCK}{PGDN}{PGUP}{PRTSC}{RIGHT}{SCROLLLOCK}{TAB}{UP}{F1}{F2}{F3}{F4}{F5}{F6}{F7}{F8}{F9}{F10}{F11}{F12}{F13}{F14}{F15}{F16}"
    Const ControlsCad = "{+}{^}{%}"
    
    Caracteres = Cadena
    ILLave = 1
    
    If Len(Caracteres) <= 0 Then Exit Function
    
    ILLave = InStr(ILLave, Caracteres, "{")
    
    If ILLave = 0 Then Exit Function
    
    FLlave = InStr(ILLave, Caracteres, "}")
    SubCaracter = UCase(Mid(Caracteres, ILLave, FLlave - ILLave + 1))
    
    'Caracteres = Replace(UCase(Caracteres), SubCaracter, "", ILLave, 1)
    
    If ILLave = 1 Then
        Caracteres = Mid(Caracteres, FLlave + 1, Len(Caracteres))
    Else
        Caracteres = Mid(Caracteres, 1, ILLave - 1) & Mid(Caracteres, FLlave + 1, Len(Caracteres))
    End If
    
    ExtraerIntervalo = SubCaracter
    
    If Eliminar Then
        Cadena = Caracteres
    End If
    
End Function

Private Function NecesitaDoble(ByRef Accion As Byte) As Boolean
    
    NecesitaDoble = False
    
    Select Case Accion
        Case vbKeyBack
            NecesitaDoble = True
        Case vbKeySpace
            NecesitaDoble = False
        Case vbKeyCancel
            NecesitaDoble = True
        Case vbKeyPause
            NecesitaDoble = True
        Case vbKeyCapital
            NecesitaDoble = True
        Case vbKeyDelete
            NecesitaDoble = True
        Case vbKeyDecimal
            NecesitaDoble = True
        Case vbKeyDown
            NecesitaDoble = True
        Case vbKeyEnd
            NecesitaDoble = True
        Case vbKeyReturn
            NecesitaDoble = False
        Case vbKeyEscape
            NecesitaDoble = True
        Case vbKeyHelp
            NecesitaDoble = True
        Case vbKeyHome
            NecesitaDoble = True
        Case vbKeyInsert
            NecesitaDoble = True
        Case vbKeyLeft
            NecesitaDoble = True
        Case vbKeyNumlock
            NecesitaDoble = True
        Case vbKeyPageDown
            NecesitaDoble = True
        Case vbKeyPageUp
            NecesitaDoble = True
        Case vbKeyPrint
            NecesitaDoble = True
        Case vbKeyRight
            NecesitaDoble = True
        Case 145
            NecesitaDoble = True
        Case vbKeyTab
            NecesitaDoble = False
        Case vbKeyUp
            NecesitaDoble = True
        Case vbKeyF1
            NecesitaDoble = True
        Case vbKeyF2
            NecesitaDoble = True
        Case vbKeyF3
            NecesitaDoble = True
        Case vbKeyF4
            NecesitaDoble = True
        Case vbKeyF5
            NecesitaDoble = True
        Case vbKeyF6
            NecesitaDoble = True
        Case vbKeyF7
            NecesitaDoble = True
        Case vbKeyF8
            NecesitaDoble = True
        Case vbKeyF9
            NecesitaDoble = True
        Case vbKeyF10
            NecesitaDoble = True
        Case vbKeyF11
            NecesitaDoble = True
        Case vbKeyF12
            NecesitaDoble = True
        Case vbKeyF13
            NecesitaDoble = True
        Case vbKeyF14
            NecesitaDoble = True
        Case vbKeyF15
            NecesitaDoble = True
        Case vbKeyF16
            NecesitaDoble = True
        Case vbKeyShift
            NecesitaDoble = True
        Case vbKeyControl
            NecesitaDoble = True
        Case 18
            NecesitaDoble = True
        Case 91
            NecesitaDoble = True
        Case 93
            NecesitaDoble = True
    End Select
    
End Function

Public Function SDecimal() As String
    
    Dim CadenaValor As String
    
    Const Valor As Long = 123.123
    
    CadenaValor = CStr(Valor)
    
    SDecimal = Mid(CadenaValor, 4, 1)
    
End Function

Public Function sGetIni(SIniFile As String, SSection As String, _
SKey As String, SDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim STemp As String * ParamMaxLength
    Dim NLength As Integer
    
    STemp = Space$(ParamMaxLength)
    
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, ParamMaxLength, SIniFile)
    
    sGetIni = VBA.Strings.Left$(STemp, NLength)
    
End Function

Public Sub sWriteIni(SIniFile As String, SSection As String, _
SKey As String, sData As String)
    WritePrivateProfileString SSection, SKey, sData, SIniFile
End Sub

Public Sub ConfigurarTeclado(KeyboardSettings)
    
    Dim Forma As Object
    Dim Opt1 As String, Opt2 As String, Opt3 As String, Opt4 As String
    Dim Result1 As String, Result2 As String
    
    Set Forma = KeyboardSettings("Forma")
    
    Forma.lbl_Organizacion.FontBold = True
    Forma.lbl_Organizacion.Caption = KeyboardSettings("TitleMsg1")
    
    Opt1 = KeyboardSettings("Opt1")
    Opt2 = KeyboardSettings("Opt2")
    
    Forma.cmdResumido.Caption = Opt1
    Forma.cmdDetallado.Caption = Opt2
    
    Forma.AccionBotonResumido = Opt1
    Forma.AccionBotonDetallado = Opt2
    
    Forma.CmdClose.Visible = True
    
    If Not Forma.Visible Then
        Forma.Show vbModal
    Else
        Exit Sub
    End If
    
    Result1 = Forma.Accion
    
    Unload Forma
    
    Set Forma = KeyboardSettings("Forma")
    
    If Result1 <> KeyboardSettings("OptCancel") Then
        
        Forma.lbl_Organizacion.FontBold = True
        Forma.lbl_Organizacion.Caption = KeyboardSettings("TitleMsg2")
        
        Opt3 = KeyboardSettings("Opt3")
        Opt4 = KeyboardSettings("Opt4")
        
        Forma.cmdResumido.Caption = Opt3
        Forma.cmdDetallado.Caption = Opt4
        
        Forma.AccionBotonResumido = Opt3
        Forma.AccionBotonDetallado = Opt4
        
        Forma.CmdClose.Visible = True
        
            If Not Forma.Visible Then
                Forma.Show vbModal
            Else
                Exit Sub
            End If
        
        Result2 = Forma.Accion
        
        Unload Forma
        
        If Result2 <> KeyboardSettings("OptCancel") Then
            
            KeyboardSettings.Remove ("CambioModoTouch")
            KeyboardSettings.Add True, "CambioModoTouch"
            
            KeyboardSettings.Remove ("ModoTouch")
            KeyboardSettings.Add (Result1 = Opt1), "ModoTouch"
            
            If Result2 = Opt3 Then
                KeyboardSettings.Remove ("GuardarModoTouch")
                KeyboardSettings.Add True, "GuardarModoTouch"
            End If
            
        End If
        
    End If
    
End Sub

Public Sub MoverVentana(hWnd As Long)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    Call ReleaseCapture
    
    lngReturnValue = SendMessage( _
    hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    
End Sub

Public Function GetTaskBarInfo() As APPBARDATA
    
    On Error Resume Next
    
    Dim ABD As APPBARDATA
    
    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarInfo = ABD
    
End Function

Public Function GetTaskBarHeight() As Long
    
    Dim ABD As APPBARDATA

    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarHeight = ABD.rc.Bottom - ABD.rc.Top

End Function

Public Function GetTaskBarWidth() As Long
    
    Dim ABD As APPBARDATA

    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarWidth = ABD.rc.Right - ABD.rc.Left

End Function

Public Function Control_TopReal(ByVal pControl As Object) As Long
    
    On Error GoTo ErrorCalc
    
    Dim CurrentContainer As Object
    
    Control_TopReal = pControl.Top
    
    Set CurrentContainer = pControl
    
    While Not (CurrentContainer Is Nothing)
        
        Set CurrentContainer = CurrentContainer.Container
        
        If Not CurrentContainer.Container Is Nothing Then
TopContainer:
            Control_TopReal = Control_TopReal + CurrentContainer.Top
        End If
            
    Wend
    
    Exit Function
    
ErrorCalc:
    
    If Err.Number = 438 Then
        Control_TopReal = Control_TopReal + CurrentContainer.Top
    End If
    
    Err.Clear
    
End Function

Public Function Control_LeftReal(ByVal pControl As Object) As Long
    
    On Error GoTo ErrorCalc
    
    Dim CurrentContainer As Object
    
    Control_LeftReal = pControl.Left
    
    Set CurrentContainer = pControl
    
    While Not (CurrentContainer Is Nothing)
        
        Set CurrentContainer = CurrentContainer.Container
        
        If Not CurrentContainer.Container Is Nothing Then
            Control_LeftReal = Control_LeftReal + CurrentContainer.Left
        End If
            
    Wend
    
    Exit Function
    
ErrorCalc:
    
    If Err.Number = 438 Then
        Control_LeftReal = Control_LeftReal + CurrentContainer.Left
    End If
    
    Err.Clear
    
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, _
Optional EsFormNavegador As Boolean = False)
    
    On Error GoTo Error
    
    Dim TbInfo As APPBARDATA
    
    TbInfo = GetTaskBarInfo
    
    If TbInfo.rc.Bottom = 0 And TbInfo.rc.Top = 0 And TbInfo.rc.Right = 0 And TbInfo.rc.Left = 0 Then
        TbInfo.uEdge = -1
    End If
    
    Select Case TbInfo.uEdge
        
        Case 3
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) - (((TbInfo.rc.Bottom - TbInfo.rc.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If Forma.Top < 0 Then
                Forma.Top = 0
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left) < 0 Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 2
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) - (((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < 0 Then
                Forma.Left = 0
            End If
            
        Case 1
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) + (((TbInfo.rc.Bottom - TbInfo.rc.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))
            
            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left < 0) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 0
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) + (((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < ((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case -1
            
            Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
            Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
            
            If Forma.Left < 0 Then Forma.Left = 0
            If Forma.Top < 0 Then Forma.Top = 0
            
    End Select
    
    ' El Form Navegador tiene un footer que siempre debe quedar en bottom.
    
    If EsFormNavegador Then
        Forma.Frame1.Top = (Forma.Height - Forma.Frame1.Height)
    End If
    
    Exit Sub
    
Error:
    
    Debug.Print Err.Description
    
End Sub

Public Sub SeleccionarTexto(pControl As Object)
    On Error Resume Next
    pControl.SelStart = 0
    pControl.SelLength = Len(pControl.Text)
End Sub
